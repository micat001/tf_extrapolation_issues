# Octomap Reconstruction

```
cd <catkin workspace>
catkin init
mkdir -p src && cd src
vcs import < tf_extrapolation_issues/repos.repos
cd hydrographic_msgs && git checkout v0.0.1
cd ..
catkin build
```

# Running

## tf transforms
To check the operation of the tf transforms, in one terminal run:

```
roslaunch batch_ros play_bag.launch config:=sonar_real bag:=/bagfile/location/real_sonar_short.bag
```

In the other:

```
roslaunch tf_extrapolation_issues tf.launch deterministic_replay:=true
```

## tf2 transforms
To check the operation of the tf2/pointcloud2 transforms:
```
roslaunch batch_ros play_bag.launch config:=sonar_real bag:=/bagfile/location/real_sonar_short.bag
```

In the other:

```
roslaunch tf_extrapolation_issues tf2.launch deterministic_replay:=true
```

## rosbag play operation
To check the operation of the tf2/pointcloud2 transforms:
```
rosbag play --clock real_sonar_short.bag
```

In the other:

```
roslaunch tf_extrapolation_issues tf.launch deterministic_replay:=false
```

# Conclusions?

When I run these tests, the first test fails and the second two succeed.