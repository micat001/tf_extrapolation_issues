// Copyright 2022 University of Washington Applied Physics Laboratory
// Author: Laura Lindzey

#include <acoustic_msgs/SonarImage.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Header.h>
#include <std_srvs/Trigger.h>
#include <tf/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>

#include <algorithm>  // std::min
#include <map>

#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"

// typedef octomap_msgs::GetOctomap OctomapSrv;
namespace sonar_map {

class ImagingSonarOctomap {
 public:
  ImagingSonarOctomap() {}
  ~ImagingSonarOctomap() {}

  void setupParameters();
  void setupSubscribers();
  void setupServices();
  virtual void setup();

 protected:
  // Helper function to find sonar's origin in map frame
  geometry_msgs::PointStamped findSonarOrigin(const std_msgs::Header& header) {
    geometry_msgs::PointStamped origin_sonar;
    origin_sonar.header = header;
    origin_sonar.point.x = 0;
    origin_sonar.point.y = 0;
    origin_sonar.point.z = 0;
    geometry_msgs::PointStamped origin_map;
    tf_listener_.transformPoint(map_frame_, origin_sonar, origin_map);
    return origin_sonar;
  }
  // Checks if message can be transformed, updates map, publishes updates.
  void handleSonarImage(const acoustic_msgs::SonarImage::ConstPtr& msg);
  // Create arc of points that correspond to the input sonar meaurement.
  // Output arc in sonar frame, where X is starboard and Z is ahead.
  sensor_msgs::PointCloud2 makeArc(float azimuth, float range,
                                   float azimuth_beamwidth,
                                   float elevation_beamwidth,
                                   const std_msgs::Header header);

  // Configuration parameters
  std::string map_frame_;  // Frame that the map will be created in

  // Flag to enable offline deterministic-replay mode.
  bool deterministic_replay_;
  int message_count_;
  ros::ServiceClient replay_client_;

  // Subscribers
  ros::Subscriber sonar_image_sub_;
  ros::Subscriber viewpoints_sub_;

  tf::TransformListener tf_listener_;

  tf2_ros::Buffer buffer_;
  std::shared_ptr<tf2_ros::TransformListener> tfl_;
};

}  // namespace sonar_map