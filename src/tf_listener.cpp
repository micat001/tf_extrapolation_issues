// Copyright 2022 University of Washington Applied Physics Laboratory
// Author: Laura Lindzey

#include "tf_extrapolation_issues/tf_listener.h"

#include <geometry_msgs/Point32.h>
#include <math.h>

#include <chrono>

namespace sonar_map {

/*
##################################
Octomap Base
##################################
*/

void ImagingSonarOctomap::setupParameters() {
  auto nh = ros::NodeHandle("~");

  if (!nh.getParam("deterministic_replay", deterministic_replay_)) {
    ROS_FATAL("Unable to find parameter 'deterministic_replay'");
    ROS_BREAK();
  }

  if (!nh.getParam("map_frame", map_frame_)) {
    ROS_FATAL("Unable to find parameter 'map_frame'");
    ROS_BREAK();
  }
}

void ImagingSonarOctomap::setupSubscribers() {
  auto nh = ros::NodeHandle("~");
  sonar_image_sub_ = nh.subscribe("/sonar_image", 1,
                                  &ImagingSonarOctomap::handleSonarImage, this);
}

void ImagingSonarOctomap::setupServices() {
  auto nh = ros::NodeHandle("~");
  if (deterministic_replay_) {
    ROS_INFO("Setting up deterministic replay service!");
    replay_client_ =
        nh.serviceClient<std_srvs::TriggerRequest>("/play_bag/trigger");
    replay_client_.waitForExistence();
    std_srvs::Trigger trig;
    replay_client_.call(trig);
  }
}

void ImagingSonarOctomap::setup() { message_count_ = 0; }

void ImagingSonarOctomap::handleSonarImage(
    const acoustic_msgs::SonarImage::ConstPtr& msg) {
  message_count_ += 1;
  ROS_WARN_STREAM("Received Message " << message_count_);
  // For deterministic_replay waitForTransform() will block indefinitely
  // because times are not being published while we wait.
  // So, don't wait.
  float transform_wait = 1.0;
  if (deterministic_replay_) {
    transform_wait = 0.0;
  }

  // ROS_WARN_STREAM(T);
  auto t0 = std::chrono::system_clock::now();
  bool can_transform = tf_listener_.waitForTransform(
      map_frame_, msg->header.frame_id, msg->header.stamp,
      ros::Duration(transform_wait));

  ROS_WARN_STREAM("Can Transform: " << can_transform);

  if (!can_transform) {
    ROS_WARN_STREAM("Dropping Sonar Image at time "
                    << msg->header.stamp.toSec());
    // Call the next new message, even if we drop the current message
    if (deterministic_replay_) {
      std_srvs::Trigger trig;
      replay_client_.call(trig);
    }
    return;
  }

  sensor_msgs::PointCloud first_arc_sonar = this->makeArc(
      msg->azimuth_angles[0], msg->ranges[0], msg->azimuth_beamwidth,
      msg->elevation_beamwidth, msg->header);

  sensor_msgs::PointCloud first_arc_map;

  tf_listener_.transformPointCloud(map_frame_, first_arc_sonar, first_arc_map);

  std::chrono::duration<double> dt = std::chrono::system_clock::now() - t0;
  ROS_WARN_STREAM("Done. final dt = " << std::fixed << std::setprecision(10)
                                      << dt.count());

  auto origin_sonar = findSonarOrigin(msg->header);
  ROS_WARN_STREAM("Found Sonar Origin");

  if (deterministic_replay_) {
    std_srvs::Trigger trig;
    replay_client_.call(trig);
  }
}

sensor_msgs::PointCloud ImagingSonarOctomap::makeArc(
    float azimuth, float range, float azimuth_beamwidth,
    float elevation_beamwidth, const std_msgs::Header header) {
  // QUESTION: Is the azimuth/elevation beamwidth the TOTAL or the HALF-WIDTH?
  // construct sensor_msgs::PointCLoud2 so I can use tf to transform it into
  // the map frame. NB: will also need to track the origin for ray tracing to
  // work correctly (but that can be done in the calling function)
  sensor_msgs::PointCloud pc;
  pc.header.frame_id = header.frame_id;
  pc.header.stamp = header.stamp;

  // Determine beam spacing such that at the maximum range our points won't be
  // separated by more than a map cell.
  double da = asin(0.01 / range);

  // Want to start at 0,0 so the arc is symmetric about the nominal beam.
  for (double el_incr = 0; el_incr <= 0.5 * elevation_beamwidth;
       el_incr += da) {
    double zz = range * sin(el_incr);
    double rr = range * cos(el_incr);

    for (double az_incr = 0; az_incr <= 0.5 * azimuth_beamwidth;
         az_incr += da) {
      std::vector<int> signs;
      if (az_incr > 0) {  // avoid duplicating point at 0 azimuth
        signs = {-1, 1};
      } else {
        signs = {1};
      }
      for (int sign : signs) {
        double xx = rr * cos(azimuth + sign * az_incr);
        double yy = rr * sin(azimuth + sign * az_incr);
        geometry_msgs::Point32 pt;
        pt.x = xx;
        pt.y = yy;
        pt.z = zz;
        pc.points.push_back(pt);
        if (el_incr > 0) {  // avoid duplicating point at 0 elevation
          pt.z = -zz;
          pc.points.push_back(pt);
        }
      }
    }
  }

  return pc;
}

}  // namespace sonar_map
