// Copyright 2022 University of Washington Applied Physics Laboratory
// Author: Laura Lindzey

#include "tf_extrapolation_issues/tf2_listener.h"

int main(int argc, char* argv[]) {
  ros::init(argc, argv, "tf2_listener");
  ros::NodeHandle nh;
  sonar_map::ImagingSonarOctomap map;
  map.setupParameters();
  map.setupSubscribers();
  map.setupServices();
  map.setup();
  // Everything is callback-based.
  ros::spin();
  return 0;
}
